(ns manifest-json
  (:refer-clojure :exclude [read-string])
  (:require[clojure.data.json :as json]
           [clojure.edn :refer [read-string]]
           [clojure.java.io :as io]
           [clojure.string :as str]))

(println "Generating manifest.json...")

(let [output-file (subs *file* 0 (- (count *file*) 4))
      proj (-> "project.clj" (slurp) (read-string))
      [_ name version] proj
      proj (->> proj (drop 3) (apply hash-map))
      [browser build-id] *command-line-args*
      build-id (or (keyword build-id) :dev)
      js-out-file-name (-> proj (get-in [:cljsbuild :builds build-id :compiler :output-to]) (io/as-file) (.getName))
      icons {:16  "icon16.png"
             :24  "icon24.png"
             :32  "icon32.png"
             :48  "icon48.png"
             :128 "icon128.png"}
      manifest (cond-> {:manifest_version 2
                        :name (str name)
                        :description (:description proj)
                        :version (first (str/split version #"-"))
                        :icons icons
                        :permissions ["tabs" "storage" "http://*/*" "https://*/*"]
                        :browser-action {:default_icon icons}
                        :background {:scripts  [js-out-file-name "background.js"] :persistent false}}
                 :default
                 (update-in [:background :scripts] #(vec (cons "polyfill.js" %)))
                 (= "chrome"  browser)
                 (assoc :options-page "option.html")
                 (= "firefox" browser)
                 (assoc :options_ui {:page "option.html"})
                 (= :dev build-id)
                 (assoc :content_security_policy
                        "script-src 'self' 'unsafe-eval'; object-src 'self'"))]
  (spit output-file
        (json/write-str manifest
                        :escape-slash false
                        :key-fn #(str/replace (clojure.core/name %) #"-" "_"))))
