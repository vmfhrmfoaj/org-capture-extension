(ns option-html
  (:require [clojure.java.io :as io]
            [hiccup.core :refer [html]]))

(println "Generating option.html...")

(let [output-file (subs *file* 0 (- (count *file*) 4))
      proj (->> "project.clj"
                (slurp)
                (read-string)
                (drop 3)
                (apply hash-map))
      [browser build-id] *command-line-args*
      build-id (or (keyword build-id) :dev)
      compile-option (get-in proj [:cljsbuild :builds build-id :compiler])
      js-out-file-name (-> compile-option :output-to (io/as-file) (.getName))]
  (->> [:html
        [:head
         [:title "org-capture options"]]
        [:body
         [:div
          [:input#new-protocol {:type "checkbox"}]
          "The version of the org-mode is higher than 9.0?"]
         [:script {:src "polyfill.js"}]
         [:script {:src js-out-file-name}]
         [:script {:src "option.js"}]]]
       (html)
       (spit output-file)))
