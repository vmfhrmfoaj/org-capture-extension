(ns org-capture.option.core)

(defn save-options []
  (let [new-protocol? (.. js/document (getElementById "new-protocol") -checked)
        options       {:new-protocol? new-protocol?}]
    (.. js/browser
        -storage
        -local
        (set (clj->js options)))))

(defn restore-options []
  (.. js/browser
      -storage
      -local
      (get (clj->js {:new-protocol? false})
           (fn [items]
             (set! (.. js/document (getElementById "new-protocol") -checked)
                   (aget items "new-protocol?"))))))

(defn ^:export index []
  (.. js/document (getElementById "new-protocol") (addEventListener "change" save-options))
  (.addEventListener js/document "DOMContentLoaded" restore-options))
