(ns org-capture.background.core
  (:require [clojure.string :as str]
            [cljs.core.async :as async :refer [<! >!]])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defonce ^:private options
  (let [opts (atom nil)
        update! (fn [& _]
                  (.. js/browser
                      -storage
                      -local
                      (get #(reset! opts (js->clj % :keywordize-keys true)))))]
    (update!)
    (.. js/browser
        -storage
        -onChanged
        (addListener update!))
    opts))

(defn encoded-str
  [s]
  (-> s
      (str/trim)
      (js/encodeURIComponent)
      (str/replace #"['()]" #(js/escape %))))

(defn toolbar-click-event-handler
  [tab]
  (go
    (let [ch (async/chan)
          _  (.. js/browser -tabs
                 (executeScript (clj->js {:code "window.getSelection().toString()"}) #(async/put! ch %)))
          selected-text (-> (<! ch) (aget 0) (encoded-str))
          url (js/encodeURIComponent (.-url tab))
          title (-> (.-title tab)
                    (str/replace #"::" "-") ; Org tag indicator => Dash
                    (encoded-str))
          org-protocol (str "org-protocol://capture"
                            (if (:new-protocol? @options)
                              (str "?template=" (if (str/blank? selected-text) "L" "p")
                                   "&url=" url
                                   "&title=" title
                                   (when-not (str/blank? selected-text)
                                     (str "&body=" selected-text)))
                              (str "/L/" url "/" title)))]
      (println "Selected text:" selected-text)
      (println "Capturing the following URI with org-protocol:" org-protocol)
      (.. js/browser -tabs
          (executeScript (clj->js {:code (str "location.href=\"" org-protocol "\"")}))))))

(defn ^:export index []
  (.. js/browser
      -browserAction
      -onClicked
      (addListener toolbar-click-event-handler)))

(enable-console-print!)
