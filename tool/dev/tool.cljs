(ns dev.tool)

(defn on-compile []
  (if-let [runtime (some-> js/browser (.-runtime))]
    (.reload runtime)
    (.reload js/location)))

(enable-console-print!)
