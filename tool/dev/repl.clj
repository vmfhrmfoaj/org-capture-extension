(ns dev.repl
  (:require [figwheel-sidecar.repl-api :as figwheel]))

(defn skip-figwheel-validation []
  (println "Patch on Figwheel.")
  (swap! @#'strictly-specking-standalone.core/registry-ref
         dissoc
         :figwheel-sidecar.schemas.config/build-config
         :figwheel-sidecar.schemas.cljs-options/compiler-options)
  (strictly-specking-standalone.core/def-key :figwheel-sidecar.schemas.config/build-config some?)
  (strictly-specking-standalone.core/def-key :figwheel-sidecar.schemas.cljs-options/compiler-options some?)
  (-> (find-ns 'figwheel-sidecar.config)
      (intern 'optimizations-none?)
      (alter-var-root (fn [f] #(or (f %) (:figwheel %))))))

(defn start []
  (skip-figwheel-validation)
  (figwheel/start-figwheel!)
  (figwheel/cljs-repl))
